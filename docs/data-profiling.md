### 데이터 프로파일링이란?
데이터 프로파일링(data profiling)은 데이터의 품질에 대한 통찰력을 얻기 위해 데이터 세트를 검사, 분석, 검토 및 요약하는 프로세스를 일컫는다. 데이터 품질은 데이터의 정확성, 완전성, 일관성, 적시성 및 접근성과 같은 요소를 기반으로 데이터의 상태를 측정하는 것이다.

또한 데이터 프로파일링에는 데이터의 구조, 내용 및 상호 관계를 이해하기 위해 소스 데이터를 검토하는 작업을 포함한다.

이 검토 프로세스는 조직에 두 가지 높은 수준의 가치를 제공한다. 하나는 데이터 세트의 품질에 대한 높은 수준의 관점을 제공하며, 다른 하나는 조직이 잠재적인 데이터 프로젝트를 찾는 데 도움을 줄 수 있다.

이러한 점을 고려할 때 데이터 프로파일링은 데이터 준비 프로그램의 중요한 구성 요소이다. 조직이 양질의 데이터를 식별할 수 있도록 지원하기 때문에 데이터 처리와 분석 활동에 앞서는 중요한 활동이다.

또한 조직은 데이터 프로파일링과 데이터의 품질을 지속적으로 개선하고 이러한 노력의 결과를 측정할 수 있도록 데이터가 제공하는 인사이트를 사용할 수 있다.

데이터 프로파일링은 데이터 고고학(archeology), 데이터 평가, 데이터 탐색(discovery) 또는 데이터 품질 분석으로도 알려져 있다.

조직은 프로젝트를 시작할 때 데이터 프로파일링을 사용하여 충분한 데이터가 수집되었는지, 데이터를 재사용할 수 있는지, 프로젝트를 수행할 가치가 있는지 여부를 결정한다. 데이터 프로파일링 프로세스 자체는 데이터 세트가 비즈니스 표준과 목표에 어떻게 부합하는지를 밝히는 특정 비즈니스 규칙에 기반을 둘 수 있다.

### 데이터 프로파일링의 유형
데이터 프로파일링에는 세 가지 유형이 있다.

- **구조 탐색**. 이는 데이터의 형식에 초점을 맞추어 모든 것이 균일하고 일관성이 있는지 확인한다. 기본 통계 분석을 사용하여 데이터의 유효성에 대한 정보를 반환한다.
- **콘텐츠 탐색**. 이 프로세스는 개별 데이터의 품질을 평가한다. 예를 들어, 모호함, 불완전함, null 등을 식별한다.
- **관계 탐색**. 이것은 데이터 소스 간의 연결, 유사성, 차이점와 연관성을 감지한다.

### 데이터 프로파일링 프로세스의 단계
데이터 프로파일링은 데이터를 분석하기에 앞서 데이터 품질 문제를 식별하고 해결하는 데 도움이 되므로 데이터 전문가는 의사 결정을 위해 데이터를 처리할 때 불일치, null 값 또는 일관성 없는 스키마 설계를 다루지 않는다.

데이터 프로파일링은 소스로부터 로드된 데이터를 통계적으로 검사하고 분석한다. 또한 메타데이터를 분석하여 정확성과 완전성을 확인한다.

일반적으로 쿼리를 작성하거나 데이터 프로파일링 도구를 사용한다.

프로세스의 개략적인 내역은 다음과 같다.

1. 데이터 프로파일링의 첫 번째 단계로 분석을 위해 하나 이상의 데이터 소스와 관련 메타데이터를 수집한다.
2. 다음 데이터를 정리하여 구조를 통합하고 중복을 제거하며 상호 관계를 식별하고 이상 징후를 찾는다.
3. 데이터가 정리되면 데이터 프로파일링 도구는 데이터 세트를 설명하는 다양한 통계를 반환한다. 여기에는 평균, 최소값/최대값, 빈도, 반복 패턴, 종속성 또는 데이터 품질 위험이 포함될 수 있다.

예를 들어, 데이터 분석가는 테이블(표)의 각 열에 대해 서로 다른 값의 빈도 분포를 조사하여 각 열의 타입과 용도에 대한 통찰력을 얻을 수 있다. 교차 열 분석(cross-column analysis)을 사용하여 내장된 값들간의 종속성을 발견할 수 있다. 테이블 간 분석을 통해 분석가는 엔티티 간의 외부 키 관계를 나타내는 겹치는 값 집합을 발견할 수도 있다.

### 데이터 프로파일링의 이점
데이터 프로파일링은 다음과 같은 이점을 제공할 수 있는 데이터의 고수준의 개요를 제공한다.

- 보다 높은 품질과 보다 신뢰할 수 있는 데이터를 추구한다.
- 보다 정확한 [예측 분석](https://searchbusinessanalytics.techtarget.com/definition/predictive-analytics?_ga=2.120978444.523829709.1681457537-336820773.1681263464&_gl=1*mp5ymr*_ga*MzM2ODIwNzczLjE2ODEyNjM0NjQ.*_ga_TQKE4GS5P9*MTY4MTQ1NzUzNy44LjEuMTY4MTQ1NzU5OC4wLjAuMA..*_ga_RZDF13FDNT*MTY4MTQ1NzUzNy43LjAuMTY4MTQ1NzUzNy4wLjAuMA..*_ga_NLDTRJGG3Y*MTY4MTQ1NzUzNy43LjAuMTY4MTQ1NzUzNy4wLjAuMA..*_ga_H4TNQB84WS*MTY4MTQ1NzUzNy43LjAuMTY4MTQ1NzUzNy4wLjAuMA..*_ga_7FK328ZGNW*MTY4MTQ1NzUzNy43LjAuMTY4MTQ1NzUzNy4wLjAuMA..)과 의사 결정을 지원한다;
- 서로 다른 데이터 세트와 소스 간의 관계를 더 잘 이해할 수 있도록 한다.
- 회사 정보를 중앙 집중식으로 유지한다;
- 결측값 또는 특이치와 같은 데이터 중심 프로젝트에 비용을 높이는 오류를 제거한다.
- 데이터 손상 또는 사용자 입력 오류와 같은 데이터 품질 문제가 가장 많이 발생하는 시스템 내 영역을 강조한다
- 리스크, 기회 및 동향에 대한 통찰력을 제공한다.

### 데이터 프로파일링 도전 과제
데이터 프로파일링의 목표는 간단하지만, 웨어하우징을 통한 데이터 수집에서 발생하는 여러 작업으로 인해 실제 관련 작업은 상당히 복잡하다.

이러한 복잡성은 조직에서 성공적인 데이터 프로파일링 프로그램을 구현하고 실행하려고 할 때 직면하는 과제 중 하나이다.

일반적으로 조직에서 수집하는 데이터의 양은 클라우드 기반 시스템에서 사물인터넷 에코시스템의 일부로 구축된 엔드포인트 장치에서 부터 데이터를 생성하는 소스의 범위에 이르기까지 다양한 과제를 갖고 있다.

데이터가 조직에 유입되는 속도는 성공적인 데이터 프로파일링 프로그램을 구축하는 데 있어 추가적인 문제를 야기한다.

이러한 [데이터 준비 과제](https://searchbusinessanalytics.techtarget.com/feature/Top-data-preparation-challenges-and-how-to-overcome-them?_ga=2.67563925.523829709.1681457537-336820773.1681263464&_gl=1*1lawri9*_ga*MzM2ODIwNzczLjE2ODEyNjM0NjQ.*_ga_TQKE4GS5P9*MTY4MTcwMTA5Ni45LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_RZDF13FDNT*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_NLDTRJGG3Y*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_H4TNQB84WS*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_7FK328ZGNW*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..)는 현대적인 데이터 프로파일링 툴을 채택하지 않고 여전히 이 작업의 대부분을 수동 프로세스에 의존하는 조직에서 훨씬 더 중요하다.

마찬가지로, 훈련된 데이터 전문가, 툴 및 그들을 위한 자금 지원을 포함하는 적절한 리소스가 없는 조직은 이러한 문제를 극복하는 데 어려움을 겪을 것이다.

그러나 이러한 동일한 요소로 인해 조직이 지능형 시스템, 고객 개인화, 생산성 향상 자동화 프로젝트 등을 지원하는 데 필요한 품질 데이터를 확보하기 위해 데이터 프로파일링이 그 어느 때보다 중요해졌다.

### 데이터 프로파일링 사례
데이터 품질이 중요한 다양한 사용 사례에서 데이터 프로파일링을 구현하고 있다.

예를 들어, [데이터 웨어하우징](https://searchdatamanagement.techtarget.com/definition/data-warehouse?_ga=2.155556287.523829709.1681457537-336820773.1681263464&_gl=1*1orkrob*_ga*MzM2ODIwNzczLjE2ODEyNjM0NjQ.*_ga_TQKE4GS5P9*MTY4MTcwMTA5Ni45LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_RZDF13FDNT*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_NLDTRJGG3Y*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_H4TNQB84WS*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_7FK328ZGNW*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..) 또는 [비즈니스 인텔리전스](https://searchbusinessanalytics.techtarget.com/definition/business-intelligence-BI?_ga=2.155556287.523829709.1681457537-336820773.1681263464&_gl=1*1orkrob*_ga*MzM2ODIwNzczLjE2ODEyNjM0NjQ.*_ga_TQKE4GS5P9*MTY4MTcwMTA5Ni45LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_RZDF13FDNT*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_NLDTRJGG3Y*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_H4TNQB84WS*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..*_ga_7FK328ZGNW*MTY4MTcwMTA5Ni44LjAuMTY4MTcwMTEwMi4wLjAuMA..)를 포함하는 프로젝트에서 하나의 보고서 또는 분석을 위해 여러 개의 다른 시스템 또는 데이터베이스에서 데이터를 수집해야 경우도 있다. 이러한 프로젝트에 데이터 프로파일링을 적용하면 ETL(Extract, Transform and Load) 작업과 기타 데이터 통합 프로세스에서 수행해야 할 잠재적인 문제와 수정 사항을 식별할 수 있다.

또한 데이터 프로파일링은 한 시스템에서 다른 시스템으로 데이터를 이동하는 것을 포함하는 데이터 변환 또는 데이터 마이그레이션 이니셔티브에서도 매우 중요하다. 데이터 프로파일링은 마이그레이션 전에 새 시스템에 수행해야 하는 변환 또는 적응 과정에서 손실될 수 있는 데이터 품질 문제를 식별하는 데 도움을 준다.

데이터 프로파일링에는 다음 네 가지 방법 또는 기법이 사용된다.

- 컬럼 프로파일링: 테이블를 평가하고 각 열의 항목을 수량화한다.
- 교차 컬럼 프로파일링: 키 분석과 종속성 분석을 피처링한다.
- 교차 테이블 프로파일링: 키 분석을 사용하여 의미론적 및 구문적 불일치뿐만 아니라 표류 데이터를 식별한다. 
- 데이터 규칙 검증: 설정된 규칙과 표준에 따라 데이터 세트를 평가하여 데이터 세트가 준수되고 있는지 확인한다.

### 데이터 프로파일링 도구
데이터 프로파일링 도구는 중복, 부정확성, 불일치 및 완전성 결여와 같은 데이터 품질에 영향을 미치는 문제를 발견하고 조사함으로써 이 기능의 수동 작업을 대부분 대체한다.

이러한 기술은 데이터 소스를 분석하고 메타데이터에 소스를 연결하여 오류에 대한 추가 조사를 허용합니다.

또한 데이터 전문가에게 데이터 품질에 대한 정량적인 정보와 통계(일반적으로 표 형식 및 그래프 형식)를 제공한다.

예를 들어, 데이터 관리 애플리케이션은 오류를 제거하고 수동 코딩 없이 여러 소스에서 추출된 데이터에 일관성을 적용하는 도구를 통해 프로파일링 프로세스를 관리할 수 있다.

이러한 툴은 오늘날 대부분은 아니지만 많은 조직에서 비즈니스 활동에 사용하는 데이터의 양이 대부분 수작업을 통해 이러한 기능을 수행하는 대규모 팀의 능력을 크게 능가하기 때문에 필수적이다.

데이터 프로파일링 도구에는 일반적으로 데이터 wrangling, 데이터 격차와 메타데이터 검색 기능뿐만 아니라 중복 항목을 탐지 및 병합하고, 데이터 유사성을 확인하고 데이터 평가를 커스텀화하는 기능도 제공한다.

데이터 프로파일링 기능을 제공하는 상용 공급업체로는 Datameer, Informatica, Oracle 및 SAS가 있으며, 오픈 소스 솔루션에는 Aggregate Profiler, Apache Griffin, Quadient DataCleaner, Talend 등이 았다.

**주** 이 페이지는 [data profiling](https://www.techtarget.com/searchdatamanagement/definition/data-profiling)를 편역한 것임.
