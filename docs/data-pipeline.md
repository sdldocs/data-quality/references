데이터 파이프라인은 데이터를 소스 위치에서 대상 위치로 이동할 수 있는 네트워크 시스템이다.

### 데이터 파이프란인의 목적

### 누가 데이터 파이프라인이 필요한가?

### 데이터 파이프라인의 동작 방법

### 데이터 파이프라인 기술의 유형

### ETL 파이프라인과 데이터 파이트라인의 차이

### 요약

**주** 이 페이지는 [data pipeline](https://www.techtarget.com/searchdatamanagement/definition/data-pipeline)를 편역한 것임.