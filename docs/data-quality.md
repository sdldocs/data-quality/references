데이터 품질은 정확성, 완전성, 일관성, 신뢰성 및 최신 상태 여부와 같은 요소를 기준으로 데이터 상태를 측정하는 것이다. 데이터 품질의 수준 측정을 통하여 조직이 해결해야 할 데이터 오류를 식별하고 IT 시스템의 데이터가 의도한 목적에 적합한지 여부를 평가할 수 있다.

데이터 처리가 비즈니스 운영과 복잡하게 연결되고 데이터 분석을 통하여 조직이 의사 결정을 주도함에 따라 엔터프라이즈 시스템에서 데이터 품질에 대한 중요성은 크게 증가하고 있다. 데이터 품질 관리는 전체 [데이터 관리](https://www.techtarget.com/searchdatamanagement/definition/data-management) 프로세스의 핵심 구성 요소이며, [데이터 품질 개선 노력](https://www.techtarget.com/searchdatamanagement/feature/Proactive-practices-for-data-quality-improvement)은 종종 데이터가 조직 전체에서 일관적으로 포맷되고 사용되도록 보장하는 것을 목표로 하는 [데이터 거버넌스](https://www.techtarget.com/searchdatamanagement/definition/data-governance) 프로그램과 밀접한 관련이 있다.

### 데이터 품질이 중요한 이유
불량 데이터는 기업에 치명적인 비즈니스 결과를 초래할 수 있다. 품질이 낮은 데이터는 종종 운영 혼란, 부정확한 분석과 잘못된 비즈니스 전략의 원인으로 꼽힌다. [경제적 손상을 입히는 데이터 품질 문제](https://www.techtarget.com/searchdatamanagement/tip/Why-businesses-should-know-the-importance-of-data-quality)의 예로는 틀린 고객 주소로 제품을 배송하여 발생하는 추가 비용, 틀린 고객 기록 또는 불완전한 고객 기록으로 인한 판매 기회 손실, 부적절한 재무 또는 규정 준수 보고에 대한 벌금 등을 꼽을 수 있다.

Gartner는 2021년에 낮은 데이터 품질로 매년 평균 1,290만 달러의 비용이 조직에 발생한다고 하였다. 여전히 자주 인용되는 또 다른 수치는 2016년 미국의 연간 데이터 품질 문제 비용이 3조 1천억 달러에 달한다는 IBM의 계산이다. 그리고 2017년 MIT Sloan Management Review에 기고된 글에서 데이터 품질 컨설턴트인 Thomas Redman은 데이터 오류를 수정하고 잘못된 데이터로 인해 발생하는 비즈니스 문제를 처리하는 데 기업이 연평균 수익의 15~25%의 비용을 지출한다고 추정했다.

더하여 BI(비즈니스 인텔리전스)와 분석 툴을 사용하여 조직의 의사 결정 과정을 개선하는 데 있어 기업 경영진과 비즈니스 관리자의 [데이터에 대한 신뢰 부족](https://www.computerweekly.com/feature/Why-some-data-driven-decisions-are-not-to-be-trusted?_gl=1*12iojpm*_ga*MzM2ODIwNzczLjE2ODEyNjM0NjQ.*_ga_TQKE4GS5P9*MTY4MTI3ODAwMC41LjEuMTY4MTI3ODAyNC4wLjAuMA..*_ga_RZDF13FDNT*MTY4MTI3ODAwMi40LjEuMTY4MTI3ODAyNC4wLjAuMA..*_ga_NLDTRJGG3Y*MTY4MTI3ODAwMi40LjEuMTY4MTI3ODAyNC4wLjAuMA..*_ga_H4TNQB84WS*MTY4MTI3ODAwMi40LjEuMTY4MTI3ODAyNC4wLjAuMA..*_ga_7FK328ZGNW*MTY4MTI3ODAwMi40LjEuMTY4MTI3ODAyNC4wLjAuMA..&_ga=2.37509280.1016152661.1681263464-336820773.1681263464)이 주요 장애 요인으로 자주 언급되고 있다. 따라서 [효과적인 데이터 품질 관리 전략](https://www.techtarget.com/searchdatamanagement/tip/Five-steps-to-an-improved-data-quality-assurance-plan)은 필수적이다. 

### 품질 좋은 데이터란?
데이터 정확성은 고품질 데이터의 핵심 속성이다. 운영 체제에서 트랜잭션 처리 문제와 분석 애플리케이션의 잘못된 결과를 방지하려면 사용되는 데이터가 정확해야 한다. 경영진, 데이터 분석가와 기타 최종 사용자가 좋은 정보를 갖고 작업할 수 있도록 부정확한 데이터를 식별하고, 문서화하고와 이를 삭제해야 한다.

[좋은 데이터 품질의 중요한 요소](https://www.techtarget.com/searchdatamanagement/tip/6-dimensions-of-data-quality-boost-data-performance)로 다음과 같은 측면 또는 차원이 포함된다.

- 완전성, 모든 데이터 요소를 포함하는 데이터 세트,
- 일관성, 다른 시스템 또는 다른 데이터 세트에서 동일한 데이터 값 간에 충돌이 발생하지 않음,
- 고유성, 데이터베이스와 [데이터 웨어하우스](https://www.techtarget.com/searchdatamanagement/definition/data-warehouse)에 저장된 데이터 레코드의 중복이 없음,
- 적시성 또는 최신성, 데이터가 최신 상태를 유지하도록 업데이트되고 필요할 때 사용할 수 있어야 함,
- 유효성, 데이터가 적절히 구조화되어 있고, 갖어야 하는 값을 포함하는 지를 확인,
- 적합성, 조직에서 생성한 표준 데이터 형식을 준수.

이러한 모든 요인을 충족하면 신뢰할 수 있고 신뢰할 수 있는 데이터 세트를 생성하는 데 도움이 된다. 데이터 품질에 대한 추가 차원의 긴 목록도 적용할 수 있다. 여기에는 적절성, 신뢰성, 관련성, 신뢰성, 유용성 등이 포함된다.

![data quality metrics](images/definition/data_quality_metrics.png)

### 데이터 품질 결정 방법
데이터 품질 수준을 결정하기 위한 첫 번째 단계로, 조직은 일반적으로 데이터 자산의 인벤토리를 작성하고 데이터 세트의 상대적 정확성, 고유성과 유효성을 측정하기 위한 기본 연구를 수행한다. 그 결과로 설정된 기본 등급을 기준으로 시스템의 데이터와 지속적으로 비교하여 데이터 품질의 새로운 문제를 식별할 수 있다.

다른 일반적인 단계는 운영과 분석 데이터 모두에 대한 비즈니스 요구사항을 기반으로 데이터 품질 규칙 집합을 만드는 것이다. 이러한 규칙은 데이터 세트에 필요한 품질 수준을 지정하고 정확성, 일관성 등 데이터 품질 속성을 확인할 수 있도록 다양한 데이터 요소를 포함해야 하는 세부 정보를 제공한다. 규칙이 마련된 후 [데이터 관리팀](https://www.techtarget.com/searchdatamanagement/tip/What-key-roles-should-a-data-management-team-include)은 일반적으로 데이터 품질 평가를 수행하여 데이터 세트의 품질을 측정하고 데이터 오류와 기타 문제를 문서화한다. 이 절차는 최대한 높은 데이터 품질 수준을 유지하기 위해 정기적으로 반복되어야 한다.

이러한 평가를 위한 다양한 방법론이 개발되었다. 예를 들어, United Health Group의 Optum 의료 서비스 자회사의 데이터 관리자는 데이터 품질 평가 방법을 공식화하기 위해 2009년에 데이터 품질 평가 프레임워크(DQAF)를 만들었다. DQAF는 완전성, 적시성, 유효성과 일관성의 네 가지 차원을 기반으로 데이터 품질을 측정하기 위한 지침을 제공한다. Optum은 프레임워크에 대한 세부 정보를 모델로 다른 조직에 적용할 수 있도록 공개했다.

세계 통화 시스템을 감독하고 경제적으로 어려운 국가에 돈을 빌려주는 국제통화기금(IMF)도 Optum과 같은 이름의 평가 방법론을 명시했다. 이 프레임워크는 회원국이 IMF에 제출해야 하는 통계 데이터의 정확성, 신뢰성, 일관성 등 데이터 품질 속성에 촛점을 맞추고 있다. 또한, U.S. government's Office of the National Coordinator for Health Information Technology은 의료 기관이 수집한 환자 인구 통계 데이터에 대한 데이터 품질 프레임워크를 자세히 설명하고 있다.

### 데이터 품질 관리 도구 및 기술
데이터 품질 프로젝트에는 일반적으로 몇 가지 다른 단계를 포함한다. 예를 들어, 데이터 관리 컨설턴트인 David Loshin이 설명한 데이터 품질 관리 주기는 불량 데이터가 비즈니스 운영에 미치는 영향을 식별하고 측정하는 것으로 시작한다. 다음으로, 데이터 품질 규칙을 정의하고, 관련 데이터 품질 메트릭을 개선하기 위한 성능 목표를 설정하고, 특정 데이터 품질 개선 프로세스를 설계하고 시행한다.

이러한 프로세스에는 데이터 오류를 수정하기 위한 [데이터 클린싱](https://www.techtarget.com/searchdatamanagement/definition/data-scrubbing) 또는 데이터 스크러빙 작업과 누락된 값, 최신 정보 또는 추가 레코드를 추가하는 등의 데이터 세트를 향상시키는 작업이 포함된다. 그런 다음 성능 목표를 기준으로 결과를 모니터링하고 측정하여, 데이터 품질에 남아 있는 결함이 있으면 다음 단계의 계획된 개선을 위한 출발점을 제공한다. 이러한 주기는 개별 프로젝트가 완료된 후에도 전반적인 데이터 품질을 개선하기 위한 노력이 계속되도록 하기 위함이다.

![데이터 품잘 관리 주기](images/definition/data_management_cycle.png)

이러한 작업을 간소화하기 위해 데이터 품질 소프트웨어 도구는 레코드를 일치시키고, 중복을 삭제하고, [새롭게 추가된 데이터를 검증하고](https://www.techtarget.com/searchdatamanagement/definition/data-validation), 교정 정책을 수립하고, 데이터 세트에서 개인 데이터를 식별할 수 있다. 또한 [데이터 프로파일링](https://www.techtarget.com/searchdatamanagement/definition/data-profiling)을 수행하여 데이터 세트에 대한 정보를 수집하고, 가능한 특이 값을 식별합니다. 증강 데이터 품질 기능은 소프트웨어 공급업체가 주로 인공지능(AI)과 기계 학습을 이용하여 작업과 절차를 자동화하기 위해 도구가 내장하고 있는 새로운 기능 세트이다.

데이터 품질 이니셔티브를 위한 관리 콘솔은 데이터 처리 규칙의 생성, 데이터 관계의 발견 및 데이터 품질 유지 관리 노력의 일부일 수 있는 자동화된 데이터 변환을 지원한다. 협업과 워크플로우 지원 툴도 보편화되어 특정 데이터 세트를 감독하는 데이터 품질 관리자와 [데이터 관리자](https://www.techtarget.com/searchdatamanagement/definition/data-stewardship)에게 기업 데이터 저장소에 대한 공유 뷰를 제공한다.

데이터 품질 도구와 개선 프로세스는 데이터 거버넌스 프로그램에 통합되는 경우가 많으며, 일반적으로 [데이터 품질 메트릭을 사용](https://www.techtarget.com/searchdatamanagement/feature/Data-governance-metrics-Data-quality-data-literacy-and-more)하여 기업에 비즈니스 가치를 입증한다. 또한 고객, 제품, 공급망 등의 데이터 도메인에 대한 마스터 데이터의 중앙 레지스트리를 생성하는 마스터 데이터 관리([MDM](https://www.techtarget.com/searchdatamanagement/definition/master-data-management): master data management) 이니셔티브의 핵심 구성 요소이기도 하다.

### 우수한 데이터 품질의 이점
재정적인 측면에서 볼 때, 높은 데이터 품질 수준을 유지함으로써 기업은 시스템에서 불량 데이터를 식별하고 수정하는 비용을 절감할 수 있다. 또한 기업은 운영 비용을 증가시키고 수익을 감소시킬 수 있는 운영 오류와 비즈니스 프로세스 중단을 피할 수 있다.

또한 데이터 품질이 우수하면 분석 애플리케이션의 정확성이 향상되므로 비즈니스 의사결정이 개선되어 매출이 증가하고 내부 프로세스가 개선되며 경쟁업체에 비해 경쟁 우위를 확보할 수 있다. 고품질 데이터를 사용하면 BI 대시보드와 분석 툴의 사용을 확대할 수 있다. 분석 데이터가 신뢰할 수 있다면 비즈니스 사용자는 직감이나 자체 스프레드시트를 기반으로 결정하는 대신 BI 대시보드에 의존할 가능성이 높아진다.

또한 효과적인 데이터 품질 관리를 통해 데이터 관리팀은 데이터 세트를 정리하는 것보다 생산적인 작업에 집중할 수 있다. 예를 들어, 비즈니스 사용자와 데이터 분석가가 시스템에서 데이터를 활용할 수 있도록 돕고 데이터 오류를 최소화하기 위해 비즈니스 운영의 데이터 품질 모범 사례를 홍보하는 데 더 많은 시간을 할애할 수 있다.

### 데이터 품질의 새로운 문제
[관계형 데이터베이스](https://www.techtarget.com/searchdatamanagement/definition/relational-database)가 데이터 관리를 위한 주요 기술이었기 때문에 수년간 데이터 품질 노력의 부담은 관계형 데이터베이스에 저장된 구조화된 데이터에 집중되었다. 그러나 [빅 데이터](https://www.techtarget.com/searchdatamanagement/definition/big-data) 시스템과 클라우드 컴퓨팅이 보편화되며 데이터 품질 문제의 성격이 확대되었다. 데이터 관리자는 텍스트, 인터넷 클릭스트림 레코드, 센서 데이터와 네트워크, 시스템과 애플리케이션 로그와 같은 비정형 및 반정형 데이터의 품질에 점점 집중하게 되었다. 또한 이제 데이터 품질은 사내 시스템과 클라우드 시스템을 함께 관리해야 하는 경우가 많이 등장했다.

조직에서 AI 도구와 [머신 러닝 애플리케이션](https://www.techtarget.com/searchenterpriseai/In-depth-guide-to-machine-learning-in-the-enterprise)의 사용이 증가함에 따라 데이터 품질 프로세스가 더욱 복잡해지고, 대량의 데이터를 지속적으로 기업 시스템에 유입하는 실시간 데이터 스트리밍 플랫폼의 채택으로 더욱 복잡해지고 있다. 데이터 과학과 고급 분석 작업을 지원하기 위해 만들어진 복잡한 [데이터 파이프라인](https://www.techtarget.com/searchdatamanagement/definition/data-pipeline)도 문제를 가중시키고 있다.

새로운 데이터 개인 정보 보호와 보호 법, 특히 유럽 연합의 일반 데이터 보호 규정([GDPR](https://www.techtarget.com/whatis/definition/General-Data-Protection-Regulation-GDPR))과 캘리포니아 소비자 개인 정보 보호법(CCPA)의 시행으로 데이터 품질에 대한 요구도 확대되고 있다. 두 규정 모두 사람들에게 기업이 수집한 개인 데이터에 액세스할 수 있는 권한을 부여하고 있다. 즉, 조직은 정확하지 않거나 일관되지 않은 데이터에서라도 시스템 내에서 개인의 어떤 기록도 찾을 수 있어야 한다.

## 데이터 품질 문제 해결
데이터 품질 관리자, 분석가와 엔지니어는 주로 조직의 데이터 오류와 기타 데이터 품질 문제를 해결해야 하는 책임이 있다. 이들은 데이터베이스와 기타 데이터 저장소에서 불량 데이터를 찾고 제거하는 작업을 공동으로 수행하며, 다른 데이터 관리 전문가, 특히 데이터 관리자와 데이터 거버넌스 프로그램 관리자의 도움과 지원을 받는 경우가 자주 발생한다.

그러나 비즈니스 사용자, 데이터 과학자와 기타 분석가를 데이터 품질 프로세스에 참여시켜 시스템에서 발생하는 데이터 품질 문제의 수를 줄이는 것도 일반적인 방법이다. 비즈니스 사용자의 참여는 데이터 거버넌스 프로그램과 비즈니스 부서 데이터 관리자와의 상호 작용을 통해 부분적으로 달성될 수 있다. 그럼에도 불구하고 많은 기업이 최종 사용자를 위한 데이터 품질 모범 사례에 대한 교육 프로그램을 운영하고 있으며, 데이터 관리자들 사이에서 흔히 오가는 말은 조직의 모든 사람이 데이터 품질에 대한 책임을 져야 한다는 것이다.

## 데이터 품질 vs. 데이터 무결성
데이터 품질과 [데이터 무결성](https://www.techtarget.com/searchdatacenter/definition/integrity)을 상호 교환적으로 사용하기도 합니다. 또는 일부에서는 데이터 무결성을 데이터 정확도의 측면 또는 데이터 품질와 별도 차원으로 취급하기도 한다. 그러나 일반적으로 데이터 무결성은 데이터 품질, 데이터 거버넌스와 데이터 보호 메커니즘을 결합하여 전체적으로 데이터 정확성, 일관성과 보안을 해결하는 광범위한 개념으로 이해되고 있다.

이러한 광범위한 관점에서 데이터 무결성은 논리적 그리고 물리적 관점 모두에서 무결성에 중점을 둔다. 논리적 무결성은 데이터 품질 측정과 참조 무결성같은 데이터베이스 속성이 포함된 서로 다른 데이터베이스 테이블의 관련 데이터 요소에 유효하다. 물리적 무결성은 무단 사용자에 의한 데이터 수정 또는 손상을 방지하기 위한 접근 제어와 기타 보안 조치뿐만 아니라 백업과 재해 복구 보호를 포함한다.

**주** 이 페이지는 [data quality](https://www.techtarget.com/searchdatamanagement/definition/data-quality)를 편역한 것임.